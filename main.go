package main

import (
    "net/http"
    "log"
    "sync"
    "html/template"
    "path/filepath"
    "flag"

    "github.com/aiki-io/trace"
    "os"
)

type templateHandler struct {
    once     sync.Once
    filename string
    templ    *template.Template
}

func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    t.once.Do(func() {
        t.templ = template.Must(template.ParseFiles((filepath.Join("templates", t.filename))))
    })
    t.templ.Execute(w, r)
}

func main() {

    var addr = flag.String("addr", ":8080", "The addr of the application")
    flag.Parse()

    r := newRoom()
    r.tracer = trace.New(os.Stdout)

    http.Handle("/chat", MustAuth(&templateHandler{filename:"chat.html"}))
    http.HandleFunc("/auth/", loginHandler)
    http.Handle("/login", &templateHandler{filename:"login.html"})

    http.Handle("/room", r)
    http.Handle("/", MustAuth(&templateHandler{filename:"chat.html"}))
    go r.run()

    log.Printf("Starting web server on: http://%s/", *addr)

    if err := http.ListenAndServe(*addr, nil); err != nil {
        log.Fatal("ListenAndServe:", err)
    }
}
